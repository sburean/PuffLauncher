package burean.szabi.pufflauncher.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * fixme:
 * this should be removed eventually, and will have a spinning animation
 * on the RecyclerView while apps load.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent startLauncher = new Intent(SplashActivity.this, PuffLauncherActivity.class);
        startActivity(startLauncher);
        finish();

    }//onCreate

}//SplashActivity
