package burean.szabi.pufflauncher.activity;

import android.support.v4.app.Fragment;

import burean.szabi.pufflauncher.fragment.PuffLauncherFragment;
import burean.szabi.pufflauncher.SingleFragmentActivity;

/**
 * Concrete activity to host a fragment that will
 * display a list of application on the device.
 */
public class PuffLauncherActivity extends SingleFragmentActivity {

    @Override
    protected Fragment getFragmentInstance() {
        return PuffLauncherFragment.newInstance();
    }

}
