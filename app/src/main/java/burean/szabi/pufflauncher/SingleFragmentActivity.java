package burean.szabi.pufflauncher;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import burean.szabi.pufflauncher.R;

/**
 * Abstract activity class for hosting a single fragment.
 */
public abstract class SingleFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResLayoutId());

        FragmentManager fm = getSupportFragmentManager();

        //see if fragment hosted by this container
        Fragment frag = fm.findFragmentById(R.id.fragment_container);
        if (frag == null) {
            fm.beginTransaction()
                    .add(R.id.fragment_container, getFragmentInstance())
                    .commit();
        }//frag hosted in container?

    }

    /**
     * Let subclasses specify which fragment subclasses of this activity should host.
     *
     * @return Fragment instance to host.
     */
    protected abstract Fragment getFragmentInstance();

    /**
     * Hook used to specify which Activity layout to inflate.
     *
     * @return Resource layout Id to inflate.
     */
    @LayoutRes
    protected int getResLayoutId() {
        return R.layout.activity_single_fragment;
    }
}
