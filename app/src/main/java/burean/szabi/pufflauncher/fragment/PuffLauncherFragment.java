package burean.szabi.pufflauncher.fragment;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import burean.szabi.pufflauncher.R;

/**
 * A fragment that displays the list of MAIN/LAUNCHER activities on the current device.
 */
public class PuffLauncherFragment extends Fragment {

    private static final String TAG = PuffLauncherFragment.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private List<ResolveInfo> mLauncherActivities;

    /**
     * Used to retrieve a new instance of this fragment, passing in any
     * required parameters.
     *
     * @return A new instance of {@link PuffLauncherFragment}
     */
    public static PuffLauncherFragment newInstance() {

//        Bundle args = new Bundle();
//        //add args here
//        PuffLauncherFragment fragment = new PuffLauncherFragment();
//        fragment.setArguments(args);
//        return fragment;

        return new PuffLauncherFragment();

    }//newInstance

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true); //retail state over screen rotation (don't re-load list again)
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View fragRootView = inflater.inflate(R.layout.fragment_puff_launcher, container, false);

        mRecyclerView = (RecyclerView) fragRootView.findViewById(R.id.puff_launcher_recycler_view);
        RecyclerView.LayoutManager llm = new LinearLayoutManager(getActivity());
        mRecyclerView.addItemDecoration(new DividerDecoration());
        mRecyclerView.setLayoutManager(llm);

        loadActivities();
        setupAdapter();

        return fragRootView;

    }//onCreateView

    /**
     * Method to set up the adapter for use by the RecyclerView
     */
    private void setupAdapter() {

        mRecyclerView.setAdapter(new LaunchActivityAdapter());

    }//setupAdapter

    /**
     * Used to populate a collection with information about launchable activities on the device.
     */
    public void loadActivities() {

        List<ResolveInfo> queryResults;
        final PackageManager pm = getActivity().getPackageManager();

        //Create an intent that can launch an activity with filter: MAIN/LAUNCHER
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN); //intent /w this action doesn't expect data.
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        queryResults = pm.queryIntentActivities(intent, 0);
        /* NOTE:
            We know resulting ResolveInfo's activityInfo field is NON-NULL since we queried
             for intent activities ! (can also query for services/providers)
         */

        //Sort the results alphabetically:
        Collections.sort(queryResults, new Comparator<ResolveInfo>() {
            @Override
            public int compare(ResolveInfo ri1, ResolveInfo ri2) {
                //Compare criteria = ResolveInfo labels (strings)

                /*
                    Return:
                        -1 if ri1 is LESS THAN ri2
                        0 if ri1 is equal to ri2 (passes ri1.equals(ri2))
                        1 if ri1 is GREATER THAN ri2
                 */
                int LESS_THAN = -1;
                int EQUAL = 0;
                int GREATER_THAN = 1;

                //First check for nulls (not really needed in a anonymous comparator, but practice)
                if (ri1 == null && ri2 == null) {
                    return EQUAL;
                } else if (ri1 == null) {
                    return LESS_THAN;
                } else if (ri2 == null) {
                    return GREATER_THAN;
                }//if else null checks

                String label1 = ri1.loadLabel(pm).toString();
                String label2 = ri2.loadLabel(pm).toString();

                //delegate comparison to the strings, ignoring case.
                return label1.compareToIgnoreCase(label2);

            }//compare
        });

        mLauncherActivities = queryResults;

    }//loadActivities

    private class LaunchActivityHolder extends RecyclerView.ViewHolder {

        private ResolveInfo mInfo;
        private TextView mLabel;
        private ImageView mIcon;

        public LaunchActivityHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //When the user clicks on a list item, launch that activity:
                    launchChosenActivity();
                }
            });

            cacheViews();

        }//LaunchActivityHolder - constructor

        /**
         * Used to store references to child views within itemView.
         */
        private void cacheViews() {

            mLabel = (TextView) itemView.findViewById(R.id.launcher_activity_text_view);
            mIcon = (ImageView) itemView.findViewById(R.id.launcher_activity_image_view);

        }//cacheViews

        private void launchChosenActivity() {

            //Explicitly launch the chosen activity IN ITS OWN TASK:
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_MAIN)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK) //start activity in its own task
                    .addCategory(Intent.CATEGORY_LAUNCHER);

            ComponentName componentToLaunch = new ComponentName(
                    mInfo.activityInfo.packageName, //package
                    mInfo.activityInfo.name //class
            );
            intent.setComponent(componentToLaunch);

            startActivity(intent);

        }//launchChosenActivity

        /**
         * Binds data to cached views in this ViewHolder.
         *
         * @param info ResolveInfo object containing information from Manifest's intent tags.
         */
        public void bindViews(ResolveInfo info) {

            PackageManager pm = getActivity().getPackageManager();

            this.mInfo = info;
            String activityLabel = mInfo.loadLabel(pm).toString();
            Drawable activityIcon = mInfo.loadIcon(pm);

            mLabel.setText(activityLabel);
            mIcon.setImageDrawable(activityIcon);

        }//bindViews

    }//LaunchActivityHolder

    private class LaunchActivityAdapter extends RecyclerView.Adapter<LaunchActivityHolder> {

        public LaunchActivityAdapter() {}

        @Override
        public LaunchActivityHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            //Called when a RecyclerView.ViewHolder needs to be created

            View listItemRootView = LayoutInflater.from(getActivity())
                    .inflate(R.layout.list_item_puff_launcher_recycler_view, parent, false);

            return new LaunchActivityHolder(listItemRootView);

        }//onCreateViewHolder

        @Override
        public void onBindViewHolder(LaunchActivityHolder holder, int position) {

            //Called when a LaunchActivityHolder needs to be populated with data.

            ResolveInfo info = mLauncherActivities.get(position);
            holder.bindViews(info);

        }//onBindViewHolder

        @Override
        public int getItemCount() {
            return mLauncherActivities.size();
        }

    }//LaunchActivityAdapter

    /**
     * Controls how to draw a divider between items of the RecyclerView.
     */
    private class DividerDecoration extends RecyclerView.ItemDecoration {

        Paint mDivider;

        /**
         * Create divider with default parameters
         */
        public DividerDecoration() {
            this(1.0f, R.color.lightGray);
        }

        /**
         * Create divider with specified parameters.
         *
         * @param dividerThickness Divider thickness as a float.
         */
        public DividerDecoration(float dividerThickness) {
            this(dividerThickness, R.color.lightGray);
        }

        /**
         * Create divider with specified parameters.
         *
         * @param dividerColorResId Divider color Resource Id.
         */
        public DividerDecoration(int dividerColorResId) {
            this(1.0f, dividerColorResId);
        }

        /**
         * Create divider with specified parameters.
         *
         * @param dividerThickness  Divider thickness as a float.
         * @param dividerColorResId Divider color Resource Id.
         */
        public DividerDecoration(float dividerThickness, int dividerColorResId) {
            mDivider = new Paint(Paint.ANTI_ALIAS_FLAG);
            mDivider.setColor(ContextCompat.getColor(getActivity(), dividerColorResId));
            mDivider.setStrokeWidth(dividerThickness);
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            super.onDrawOver(c, parent, state);

            RecyclerView.LayoutManager lm = parent.getLayoutManager();

            for (int i = 0; (i < parent.getChildCount() - 1); i++) { // -1 to skip last child
                View childView = parent.getChildAt(i);

                //draw a straight horizontal line below each child view, skipping last child of list
                c.drawLine(
                        lm.getDecoratedLeft(childView),//startX
                        lm.getDecoratedBottom(childView),//startY
                        lm.getDecoratedRight(childView),//stopX
                        lm.getDecoratedBottom(childView),//stopY
                        mDivider//paint-item-to-draw
                );

            }//foreach child in the RecyclerView

        }//onDrawOver

    }//DividerDecoration

}//PuffLauncherFragment
